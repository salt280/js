/* 
Теоретичні питання
1. Опишіть своїми словами що таке Document Object Model (DOM)

DOM - document object model, це інтерфейс, що дозволяє розробникам маніпулювати вмістом,
структурою та стилем веб сайту. Представляє весь контент сторінки як обʼєкти,
має ієрархічну деревоподібну модель.


2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?

innerHTML - надає можливість вставляти HTML-теги в елемент, innerText вставлятиме все у
вигляді рядків.


3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?

За допомогою:
- getElementById
- getElementsByClassName або querySelector
- getElementsByTagName
- querySelectorAll
- name або querySelector з атрибутами.

Не має кращого, все залежть від завдання, що потрібно виконати.


4. Яка різниця між nodeList та HTMLCollection?

Два різних типи об'єктів, які представляють колекції елементів в DOM.
NodeList може містити будь-які вузли (текстові, коментарі, елементи тощо), 
тоді як HTMLCollection обмежений лише елементами HTML-документа.



Практичні завдання
 1. Знайдіть всі елементи з класом "feature", запишіть в змінну, вивести в консоль.
 Використайте 2 способи для пошуку елементів. 
 Задайте кожному елементу з класом "feature" вирівнювання тексту по - центру(text-align: center).
 2. Змініть текст усіх елементів h2 на "Awesome feature".
 3. Знайдіть всі елементи з класом "feature-title" 
 та додайте в кінець тексту елементу знак оклику "!".
 */


 let searchFeature = document.getElementsByClassName("feature");
 console.log(searchFeature);

 let searchFeature2 = document.querySelectorAll(".feature");
 console.log(searchFeature2);


for (let i = 0; i < searchFeature.length; i++){
   searchFeature[i].style.textAlign = 'center'; 
}

let h2 = document.getElementsByTagName("h2");

for (let i = 0; i < h2.length; i++) {
    h2[i].textContent = "Awesome feature";
}

let searchFeatureTitle = document.getElementsByClassName("feature-title");
for(let i = 0; i < searchFeatureTitle.length; i++){
    searchFeatureTitle[i].textContent = h2[i].textContent +'!'};


