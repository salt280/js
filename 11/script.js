/* 
Теоретичні питання
1. Що таке події в JavaScript і для чого вони використовуються?


Події в JavaScript - це взаємодія браузера або іншого середовища з веб-сторінкою
або додатком. Вони представляють різні дії, такі як кліки мишею, натискання клавіш, 
завантаження сторінки, зміна розміру вікна, ввод тексту і так далі. 
Події використовуються для того, щоб реагувати на взаємодію користувача з веб-сторінкою
та запускати відповідні функції або скрипти.



2. Які події миші доступні в JavaScript? Наведіть кілька прикладів.


click: Викликається при кліку лівою кнопкою миші.
dblclick: Викликається при подвійному кліку лівою кнопкою миші.
mousedown: Викликається при натисканні будь-якої кнопки миші.
mouseup: Викликається при відпусканні будь-якої кнопки миші.
mousemove: Викликається при русі миші над елементом.
mouseover та mouseout: Викликаються при наведенні миші на елемент або 
при виведенні миші з елемента.



3. Що таке подія "contextmenu" і як вона використовується для контекстного меню?


Подія "contextmenu" виникає, коли користувач викликає контекстне меню (контекстний клік правою кнопкою миші) 
на елементі сторінки чи об'єкті.

Приклад:

element.addEventListener('contextmenu', function(event) {
    Запобігти виведенню стандартного контекстного меню браузера
    event.preventDefault();

    Код для обробки події contextmenu
    console.log('Контекстне меню викликано');
});

Ця подія може бути корисною для створення власного контекстного меню або для виклику 
певних дій при контекстному кліку правою кнопкою миші.



Практичні завдання
 1. Додати новий абзац по кліку на кнопку:
  По кліку на кнопку <button id="btn-click">Click Me</button>,
  створіть новий елемент <p> з текстом "New Paragraph" і 
  додайте його до розділу <section id="content">
 
 2. Додати новий елемент форми із атрибутами:
 Створіть кнопку з id "btn-input-create", додайте її на сторінку в section перед footer.
  По кліку на створену кнопку, створіть новий елемент <input> і 
  додайте до нього власні атрибути, наприклад, type, placeholder, і name. 
  та додайте його під кнопкою.
 */

  const btn = document.querySelector("#btn-click");
  const sectionContent = document.querySelector("#content");
  
  btn.addEventListener("click", () => {
      const p = document.createElement("p");
      p.textContent = "New Paragraph";
      btn.after(p);
  });
  
  const container = document.createElement("div");
  container.style.display = "flex";
  container.style.flexDirection = "column";
  container.style.alignItems = "center";
  
  const btn2 = document.createElement("button");
  btn2.id = "btn-input-create";
  btn2.textContent = "Натисніть мене";
  btn2.style.width = "91.08px";
  btn2.style.height = "35.74px";
  btn2.style.backgroundColor = "red";
  
  function onButtonClick() {
      const input = document.createElement("input");
      input.style.width = "500px";
      input.size = "100";
      input.type = "tel";
      input.placeholder = "100";
      input.style.color = "red";
      input.style.backgroundColor = "yellow";
      container.after(input);
  }
  
  btn2.addEventListener("click", onButtonClick);
  

  sectionContent.append(container);
  container.append(btn2);

// Над footer:
// sectionContent.after(container);
// container.append(btn2);
  