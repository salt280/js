"use strict"

// 1. Опишіть своїми словами, що таке метод об'єкту


// Методи - це функції, 
// які зберігаються у властивостях обʼєктів, для виконання дій над ними. 



// 2. Який тип даних може мати значення властивості об'єкта?


// - Число / Number;
// - Рядок / String;
// - Масив / Array;
// - Логічний тип / Boolean;
// - Функцію / Function;
// - Інший обʼєкт / Object.



// 3. Об'єкт це посилальний тип даних. Що означає це поняття?


// Посилальний тип – це внутрішній тип мови.
// Коли створити змінну об'єкту в JavaScript, 
// вона не містить сам об'єкт, а лише посилання 
// на об'єкт у пам'яті. Це відрізняється від "примітивних типів даних" 
// (таких як числа, рядки, булеві значення), 
// які зберігаються безпосередньо у змінній.

// Коли скопіювати об'єкт або передати його як аргумент функції, 
// копіюється лише посилання на об'єкт, а не сам об'єкт.
// Це означає, що обидві змінні вказують на один і той же об'єкт у пам'яті.
// Якщо змінити властивості об'єкта через одну змінну,
// це відобразиться і на інших змінних, які вказують на той самий об'єкт.



// 1. Створіть об'єкт product з властивостями name, 
// price та discount. Додайте метод для виведення повної ціни товару з
// урахуванням знижки. Викличте цей метод та результат виведіть в консоль.



// let product = {};

// Object.defineProperties(product, {

//   name: {
//     value: "Nokia"
//   },
//   price: {
//     value: 1000
//   },
//   discount: {
//     value: 25
//   },
//   discountPrice: {
//   get: function () {
//     return this.price - (this.price * (this.discount / 100))
//   }
// }
// });

// console.log("The final price of " + product.name + " is " + product.discountPrice);



// 2. Напишіть функцію, яка приймає об'єкт з властивостями name та age,
// і повертає рядок з привітанням і віком, наприклад "Привіт, мені 30 років".
// Попросіть користувача ввести своє ім'я та вік за допомогою prompt,
// і викличте функцію з введеними даними.
// Результат виклику функції виведіть з допомогою alert.



function generateGreeting({ name, age }) {
    return `Привіт, мені ${age} років.`;
  }

  let userName = prompt("Enter your name: ");
  let userAge = prompt("Enter your age: ");

  const user = { name: userName, age: userAge };
  
  let greetingMessage = generateGreeting(user);
  alert(greetingMessage);
  




// 3.Опціональне. Завдання:
// Реалізувати повне клонування об'єкта.


// const person2 = {};

// for(const key in person) {
//   person2[key] = person[key];
// }

// console.log(person);
// console.log(person2);


//////////////////////// або за допомогою deepClone/////////////////////////


// function deepClone(obj) {
//   if (obj === null || typeof obj !== 'object') {
//     return obj;
//   }

//   if (Array.isArray(obj)) {
//     const newArray = [];
//     for (let i = 0; i < obj.length; i++) {
//       newArray[i] = deepClone(obj[i]);
//     }
//     return newArray;
//   }

//   const newObj = {};
//   for (const key in obj) {
//     if (obj.hasOwnProperty(key)) {
//       newObj[key] = deepClone(obj[key]);
//     }
//   }

//   return newObj;
// }
// const person2 = deepClone(person);

// console.log(person2);
// console.log(person)