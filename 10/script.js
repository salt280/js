/* 
Теоретичні питання
1. Які способи JavaScript можна використовувати для створення та додавання нових DOM-елементів?

Створити можна за допомогою методу document.createElement(""), 
innerHTML або outerHTML, document.createDocumentFragment(), cloneNode().



2. Опишіть покроково процес видалення одного елементу (умовно клас "navigation") зі сторінки.

- знайти елемент, getElementById, getElementsByClassName, querySelector...
- використати змінну, на яку записано елемент і додати до методу remove()

 let navigation = document.querySelector(".navigation");
 navigation.remove();



3. Які є методи для вставки DOM-елементів перед/після іншого DOM-елемента?

Методи додавання елементів:

before();
after();
prepend();
append(); + застарілий appendChild();

insertAdjacentElement("перший аргумент, куди додати *", другий аргумент - елемент)

* - afterbegin, afterend, beforebegin, beforeend.

insertAdjacentHTML("","") - тільки для роботи з рядками розуміє текст з розміткою.
insertAdjacentText("","") - тільки для роботи з рядками.



Практичні завдання
 1. Створіть новий елемент <a>, задайте йому текст "Learn More" і
 атрибут href з посиланням на "#". Додайте цей елемент в footer після параграфу.
 
 2. Створіть новий елемент <select>. Задайте йому ідентифікатор "rating", 
 і додайте його в тег main перед секцією "Features".

 Створіть новий елемент <option> зі значенням "4" і текстом "4 Stars", і додайте його до списку вибору рейтингу.
 Створіть новий елемент <option> зі значенням "3" і текстом "3 Stars", і додайте його до списку вибору рейтингу.
 Створіть новий елемент <option> зі значенням "2" і текстом "2 Stars", і додайте його до списку вибору рейтингу.
 Створіть новий елемент <option> зі значенням "1" і текстом "1 Star", і додайте його до списку вибору рейтингу.
 */

 const footer = document.querySelector("footer");

 const a = document.createElement("a");
 a.textContent = "Learn More";
 a.href = "#";

footer.append(a);

const main = document.querySelector("main");
const select = document.createElement("select");
select.id = "rating";

main.insertAdjacentElement("afterbegin", select);

for(let i = 4; i >= 1; i--) {
    const option = document.createElement("option");
    option.textContent = i === 1 ? `${i} star` : `${i} stars`;
        option.value = `${i}`;
        select.append(option);
}



